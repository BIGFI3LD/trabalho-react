import { EButton } from "./style";



interface ButtonInfo {
    param: string;

}

export function EnterButton({ param }: ButtonInfo) {
    return (
        <EButton placeholder={param} />

    );

}