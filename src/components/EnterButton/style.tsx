import styled from "styled-components/native";


export const EButton = styled.Button`
position: absolute;
width: 340px;
height: 93px;
marginLeft: 0px;
marginTop: 0px;

background: #4A91D2;
box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
border-radius: 20px;

`;