import { TextBox } from "./style";


interface TextInfo {
    param: string;

}
function InputTextLogin({ param }: TextInfo) {
    return (
        <TextBox placeholder={param} />

    );

}


export default InputTextLogin;