import React from 'react';
import { Text } from 'react-native';
import AppLoading from 'expo-app-loading';
import { useFonts, JockeyOne_400Regular } from '@expo-google-fonts/jockey-one';
import { Container, ImageLogin, LogoDis, LogoRec, Title } from './style';
import InputTextLogin from '../../components/InputTextLogin';
import { EnterButton } from '../../components/EnterButton';


function Login() {
    let [fontsLoaded, error] = useFonts({ JockeyOne_400Regular })

    if (!fontsLoaded) {
        return <AppLoading />

    }

    return (
        <Container>
            <LogoRec>
                <Title>Reus-e</Title>
                <ImageLogin source={require('../../assets/reuse-logo.png')} />
                <LogoDis>Preservando o Meio Ambiente e o seu Dinheiro</LogoDis>
            </LogoRec>

        </Container>

    );

}


export default Login;